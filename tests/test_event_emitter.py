import pytest
from event_emitter import EventEmitter


@pytest.fixture
def event_emitter() -> EventEmitter:
    return EventEmitter()


def test_listen_events_given_event_name_and_listener(mocker, event_emitter: EventEmitter):
    event_name = "my-event"
    listener_stub = mocker.stub(name="listener")

    event_emitter.listen(event_name, listener_stub)
    event_emitter.emit(event_name)
    event_emitter.emit(event_name)

    assert listener_stub.call_count == 2


def test_listen_multiple_events(mocker, event_emitter: EventEmitter):
    event_name_a = "my-event-a"
    listener_stub_a = mocker.stub(name="listener_a")
    event_name_b = "my-event-b"
    listener_stub_b = mocker.stub(name="listener_b")

    event_emitter.listen(event_name_a, listener_stub_a)
    event_emitter.listen(event_name_b, listener_stub_b)
    event_emitter.emit(event_name_a)
    event_emitter.emit(event_name_b)

    assert listener_stub_a.call_count == 1
    assert listener_stub_b.call_count == 1


def test_add_multiple_listeners_same_event(mocker, event_emitter: EventEmitter):
    event_name = "my-event"
    listener_stub_a = mocker.stub(name="listener_a")
    listener_stub_b = mocker.stub(name="listener_b")

    event_emitter.listen(event_name, listener_stub_a)
    event_emitter.listen(event_name, listener_stub_b)
    event_emitter.emit(event_name)

    assert listener_stub_a.call_count == 1
    assert listener_stub_b.call_count == 1


def test_pass_data_to_listeners_when_emitting_events(mocker, event_emitter: EventEmitter):
    event_name = "my-event"
    data = {"foo": "bar"}
    listener_stub = mocker.stub(name="listener")

    event_emitter.listen(event_name, listener_stub)
    event_emitter.emit(event_name, data)

    assert listener_stub.call_count == 1
    listener_stub.assert_called_once_with(data)


def test_remove_event_listener(mocker, event_emitter: EventEmitter):
    event_name = "my-event"
    listener_stub = mocker.stub(name="listener")

    event_emitter.listen(event_name, listener_stub)
    event_emitter.remove_event(event_name)
    event_emitter.emit(event_name)

    assert listener_stub.call_count == 0


def test_handle_listener_error(mocker, event_emitter: EventEmitter):
    def listener_error():
        raise Exception()

    event_name = "my-event"
    listener_stub = mocker.stub(name="listener")

    event_emitter.listen(event_name, listener_stub)
    event_emitter.listen(event_name, listener_error)
    event_emitter.emit(event_name)

    assert listener_stub.call_count == 1


def test_add_listeners_triggered_once(mocker, event_emitter: EventEmitter):
    event_name = "my-event"
    listener_stub_a = mocker.stub(name="listener_a")
    listener_stub_b = mocker.stub(name="listener_b")
    listener_stub_c = mocker.stub(name="listener_c")

    event_emitter.listen(event_name, listener_stub_a)
    event_emitter.listen_once(event_name, listener_stub_b)
    event_emitter.listen_once(event_name, listener_stub_c)
    event_emitter.emit(event_name)
    event_emitter.emit(event_name)

    assert listener_stub_a.call_count == 2
    assert listener_stub_b.call_count == 1
    assert listener_stub_c.call_count == 1


def test_remove_all_events(mocker, event_emitter: EventEmitter):
    event_name_a = "my-event-a"
    listener_stub_a = mocker.stub(name="listener_a")
    event_name_b = "my-event-b"
    listener_stub_b = mocker.stub(name="listener_b")

    event_emitter.listen(event_name_a, listener_stub_a)
    event_emitter.listen(event_name_b, listener_stub_b)
    event_emitter.remove_all_events()
    event_emitter.emit(event_name_a)
    event_emitter.emit(event_name_b)

    assert listener_stub_a.call_count == 0
    assert listener_stub_b.call_count == 0
