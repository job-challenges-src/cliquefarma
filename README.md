# Coding Challenge: EventEmitter

## O problema

Crie uma lib `EventEmitter` usando python.

Com ela é possível escutar e emitir eventos.

## API esperada:
- `listen`: Deve adicionar um listener para o event_name;
- `emit`: Deve emitir eventos, trigando seus listeners;
- `remove_event`: Deve remover os listeners desse event_name;
- Eventos podem ser emitidos com dados, que serão repassados para os listeners;
- `listen_once`: Semelhante ao `listen`, entretanto o callback será trigado apenas uma vez;
- `remove_all_events`: Deve remover todos os eventos e listeners

## Exemplo:

```python
event_emitter = EventEmitter()

event_emitter.listen("my-awesome-event", do_something)
event_emitter.listen("my-awesome-event", do_something_else)

event_emitter.emit("my-awesome-event")
event_emitter.emit("my-awesome-event", { "foo": "bar" })

event_emitter.remove_event("my-awesome-event")
```

## Expectativa de solução

- Construir a lib eventemitter, para passar nos testes automatizados.
- Os primeiros testes são os mais importantes.
- Existem bibliotecas e artigos na internet que solucionam o problema, mas elas não devem ser usadas.
- Conte com a ajuda dos entrevistadores para qualquer dúvida.
- Tudo bem nunca ter visto esse problema antes, pergunte a vontade.
- O live coding é mais sobre como resolver o problema, e menos sobre conhecimento prévio.
- Não há resposta correta.
- Problemas podem ser resolvidos de diferentes formas.
- Fique a vontade para trazer sua solução. =)

## Arquivo de tests
Junto ao desafio, há um arquivo de testes para verificar o funcionamento do código.

## Rodando os testes

```bash
// Instalando as dependencias
pip install -r requirements.txt

// Rodando os testes
pytest
```
