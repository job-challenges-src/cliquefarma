

from typing import List, Callable, Dict, Tuple


class EventEmitter:

    __events: Dict[str, List[Tuple[Callable, bool]]] = {}

    def events_exists(self, event_name: str) -> bool:
        return event_name in self.__events

    def listen(self, event_name: str, callback: Callable, once: bool = False):
        if not self.events_exists(event_name):
            self.__events[event_name] = []

        self.__events[event_name].append((callback, once))
    
    def listen_once(self, event_name: str, callback: Callable):
        self.listen(event_name, callback, once=True)
    
    def emit(self, event_name: str, *args, **kwargs):
        if not self.events_exists(event_name):
            return

        for callback, _ in self.__events[event_name]:
            try:
                callback(*args, **kwargs)
            except Exception:
                ...
        self.__events[event_name] = list(filter(lambda item: item[1] == False, self.__events[event_name]))

    
    def remove_event(self, event_name: str):
        if not self.events_exists(event_name):
            return
        del self.__events[event_name]
    
    def remove_all_events(self):
        self.__events = {}
